﻿using UnityEngine;
using Cinemachine;
public class RoundCameraPos : CinemachineExtension
{
    public float PixelsPerUnit = 32;
    protected override void PostPipelineStageCallback(CinemachineVirtualCameraBase vcam, CinemachineCore.Stage stage, ref CameraState state, float deltaTime)
    {
        if (stage == CinemachineCore.Stage.Body)
        {
            Vector3 pos = state.FinalPosition;
            print("pos:"+pos);
            Vector3 newPos = new Vector3(Round(pos.x), Round(pos.y), pos.z);
            print("newPos:"+newPos);
            state.PositionCorrection += newPos - pos;
        }
    }
    float Round(float x)
    {
        // return Mathf.Round(x );
        return Mathf.Round(x * PixelsPerUnit) / PixelsPerUnit;
    }

}
