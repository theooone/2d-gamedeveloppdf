﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arc : MonoBehaviour
{   
    Vector3 destination;
    public IEnumerator TravelArc(Vector3 destination, float duration)
    {
        var startPosition = transform.position;
        var percentComplete = 0.0f;
        this.destination=destination;
        while (percentComplete <= 1.0f)
        {
            // Time.deltaTime is the time elapsed since the last frame was drawn
            percentComplete += Time.deltaTime / duration;
            var currentHeight = Mathf.Sin(Mathf.PI * percentComplete);
            transform.position = Vector3.Lerp(startPosition, destination, percentComplete) + Vector3.up * currentHeight;
            print("transform.position:"+transform.position);
            yield return null;
        }
        transform.position=Vector3.zero;
        // if arc has completed, deactivate
        gameObject.SetActive(false);

    }
    private void Update() {
              
    }
        void OnDrawGizmos()
    {
        if (gameObject != null)
        {
            Gizmos.DrawLine(transform.position, destination);
            Gizmos.DrawSphere(transform.position, 1);
      
        }
    }
}
