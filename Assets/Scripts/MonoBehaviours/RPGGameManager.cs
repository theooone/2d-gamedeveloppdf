﻿
using System.Collections;
using UnityEngine;

public class RPGGameManager : MonoBehaviour
{
    public RPGCameraManager cameraManager;
    public SpawnPoint playerSpawnPoint;

    Coroutine Add1;
    private void Awake()
    {
        //   Add1=  StartCoroutine(Add());

    }

    private IEnumerator Add()
    {
        while (true)
        {
            yield return null;
            print("IEnumerator");
        }
    }

    void Start()
    {
        SetupScene();
    }
    private void SetupScene()
    {
        SpawnPlayer();
    }
    public void SpawnPlayer()
    {
        if (playerSpawnPoint != null)
        {
            GameObject player = playerSpawnPoint.SpawnObject();
            cameraManager.virtualCamera.Follow = player.transform;
        }
    }

    public void StopCoroutineButton()
    {
        StopCoroutine(Add1);
    }
    public void Coroutine2NullButton()
    {
        Add1 = null;
    }
    private void Update()
    {
        if (Input.GetKey("escape"))
            Application.Quit();
    }
}

