﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    public float movementSpeed = 3.0f;
    Vector2 movement = new Vector2();
    Rigidbody2D rb2D;
    Animator animator;

    void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        int a = 7;
        int b = a >> 1;
        print(b);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateState();
    }

    private void UpdateState()
    {
        if (Mathf.Approximately(movement.x, 0) && Mathf.Approximately(movement.y, 0))
        {
            animator.SetBool("isWalking", false);
        }
        else
        {
            animator.SetBool("isWalking", true);
        }

        animator.SetFloat("xDir", movement.x);
        animator.SetFloat("yDir", movement.y);
        // if (movement.x > 0)
        // {
        //     animator.SetInteger(animationState, (int)CharStates.walkEast);
        // }
        // else if (movement.x < 0)
        // {
        //     animator.SetInteger(animationState, (int)CharStates.walkWest);
        // }
        // else if (movement.y > 0)
        // {
        //     animator.SetInteger(animationState, (int)CharStates.walkNorth);
        // }
        // else if (movement.y < 0)
        // {
        //     animator.SetInteger(animationState, (int)CharStates.walkSouth);
        // }
        // else
        // {
        //     animator.SetInteger(animationState, (int)CharStates.idleSouth);
        // }
    }

    private void FixedUpdate()
    {
        MoveChaacter();
    }

    private void MoveChaacter()
    {
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.y = Input.GetAxisRaw("Vertical");
        movement.Normalize();
        rb2D.velocity = movement * movementSpeed;
    }
}
