﻿using UnityEngine;

public class Ammo : MonoBehaviour
{
    Arc arc;
    public Coroutine coroutine;
    private void Start() {
        arc=GetComponent<Arc>();
    }
  public int damageInflicted;

    void OnTriggerEnter2D(Collider2D collision)
    {
        // Check if we're colliding with the BoxCollider2D surrounding the Enemy,
        // Necessary since we also have a CircleCollider2D used for the Wander script,
        // and we don't care if the Ammo collides with that collider.
        if (collision is BoxCollider2D)
        {
            Enemy enemy = collision.gameObject.GetComponent<Enemy>();
            if(!gameObject.activeSelf) return;
            StartCoroutine(enemy.DamageCharacter(damageInflicted, 0.0f));
            gameObject.SetActive(false);
            // StopCoroutine(arc.TravelArc(Vector3.one, 0.0f));
            if(arc!=null&&coroutine!=null)
            StopCoroutine(coroutine);
        }
    }
}
